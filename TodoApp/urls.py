
from django.contrib import admin
from django.urls import path
from TodoApp import views
urlpatterns = [
    path('', views.home,name='home'),
    path('CreateTodo/', views.CreateTodo,name='create-todo'),
    path('DisplayTodo/', views.DisplayTodo,name='display-todo'),
    path('DeleteTodo/<int:pk>/', views.DeleteTodo,name='delete-todo'),
    path('EditTodo/<int:pk>/', views.EditTodo,name='edit-todo'),
]
