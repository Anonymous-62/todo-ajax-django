from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.template.loader import render_to_string
# Create your views here.
def home(request):
    return render(request,'TodoApp/home.html',{})

def CreateTodo(request):
    data=dict()
    if request.method == 'POST':
        text=request.POST.get('text')
        User.objects.create(username=text)
        data['status']=True
        return JsonResponse(data)
    
def DisplayTodo(request):
    data=dict()
    users=User.objects.all().order_by('-pk')
    data['partial_data']=render_to_string('TodoApp/partial_data.html',{'users':users},request=request)
    return JsonResponse(data)

def DeleteTodo(request,pk):
    User.objects.get(pk=pk).delete()
    return JsonResponse({'status':'record deleted successfully'})

def EditTodo(request,pk):
    data=dict()
    user=User.objects.get(pk=pk)
    if request.method=='POST':
        text=request.POST.get('text')
        print(text)
        user.username=text
        user.save()
        return JsonResponse({'status':'Data updated successfully'})
    data['edit_form']=render_to_string('TodoApp/edit_form.html',{'user':user},request=request)
    return JsonResponse(data)
    
